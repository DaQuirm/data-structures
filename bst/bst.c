#include "bst.h"

void bst_insert(bstnode **tree, void *data, int key) {
	bstnode *target = *tree, *node, **ptarget = tree;
	node = (bstnode*)malloc(sizeof(bstnode));
	node->key = key;
	node->data = data;
	node->left = node->right = NULL;

	while(target != NULL) {
		if (key < target->key) {
			ptarget = &target->left;
			target = target->left;

		} else {
			ptarget = &target->right;
			target = target->right;
		}
	}

	*ptarget = node;
}

bstnode* bst_find(bstnode *tree, int key) {
	bstnode *target = tree;
	while(target != NULL && target->key != key) {
		if (key < target->key) {
			target = target->left;

		} else {
			target = target->right;
		}
	}
	return target;
}

void bst_delete(bstnode *tree, int key) {
	bstnode *target = bst_find(tree, key);
	if (target != NULL) {
		free(target);
	}
}
