#include <stdio.h>
#include <stdlib.h>

struct _bstnode {
   int key;
   void *data;
   struct _bstnode *left, *right;
};

typedef struct _bstnode bstnode;

void bst_insert(bstnode **tree, void *data, int key);
bstnode* bst_find(bstnode *tree, int key);
void bst_delete(bstnode *tree, int key);
