#include <stdio.h>
#include <glib.h>
#include "../bst.h"

typedef struct {
    bstnode *tree;
} bstfixture;

void bst_setup(bstfixture *bstf, gconstpointer test_data) {
	bstf->tree = NULL;
	bst_insert(&bstf->tree, "10", 10);
	bst_insert(&bstf->tree, "5", 5);
	bst_insert(&bstf->tree, "3", 3);
	bst_insert(&bstf->tree, "4", 4);
	bst_insert(&bstf->tree, "12", 12);
	bst_insert(&bstf->tree, "6", 6);
}

void bst_teardown(bstfixture *bstf, gconstpointer test_data) {
	free(bstf->tree);
}

void test_insert(bstfixture *bstf, gconstpointer ignored) {
	g_assert_cmpint(bstf->tree->key, ==, 10);
	g_assert_cmpint(bstf->tree->left->key, ==, 5);
	g_assert_cmpint(bstf->tree->left->left->key, ==, 3);
	g_assert_cmpint(bstf->tree->left->left->right->key, ==, 4);
	g_assert_cmpint(bstf->tree->right->key, ==, 12);
	g_assert_cmpint(bstf->tree->left->right->key, ==, 6);
}

void test_find(bstfixture *bstf, gconstpointer ignored) {
	bstnode *node;
	node = bst_find(bstf->tree, 6);
	g_assert_cmpstr((char*)node->data, ==, "6");
	node = bst_find(bstf->tree, 12);
	g_assert_cmpstr((char*)node->data, ==, "12");
	node = bst_find(bstf->tree, 13);
	g_assert(node == NULL);
}

void test_delete(bstfixture *bstf, gconstpointer ignored) {
	bstnode *node;
	node = bst_find(bstf->tree, 5);
	g_assert_cmpstr((char*)node->data, ==, "5");
	bst_delete(bstf->tree, 5);
	node = bst_find(bstf->tree, 5);
	g_assert(node == NULL);
}

int main(int argc, char **argv) {
    g_test_init(&argc, &argv, NULL);
    g_test_add("/bst/test_insert", bstfixture, NULL, bst_setup, test_insert, bst_teardown);
    g_test_add("/bst/test_find", bstfixture, NULL, bst_setup, test_find, bst_teardown);
    g_test_add("/bst/test_delete", bstfixture, NULL, bst_setup, test_delete, bst_teardown);
    return g_test_run();
}
