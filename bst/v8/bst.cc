#include <v8.h>
#include <node.h>
#include <vector>

extern "C" {
	#include "../bst.h"
}

using namespace v8;

bstnode* tree;
std::vector< Persistent<Object> > refs(256);

Handle<Value> Insert(const Arguments& args) {
	HandleScope scope;
	Handle<Object> data = Handle<Object>::Cast(args[1]);
	refs.push_back(Persistent<Object>::New(data));
	if (refs.size() == refs.capacity()) {
		refs.reserve(refs.capacity()*2);
	}
	bst_insert(&tree, &refs.at(refs.size()-1), args[0]->NumberValue());
	return scope.Close(Undefined());
}

Handle<Value> Find(const Arguments& args) {
	HandleScope scope;
	bstnode *node =	bst_find(tree, args[0]->NumberValue());
	Persistent<Object> data = *(Persistent<Object>*)(node->data);
	Handle<Object> result = Object::New();
	result->Set(String::NewSymbol("key"), Number::New(node->key));
	result->Set(String::NewSymbol("data"), data);
	return scope.Close(result);
}

void init(Handle<Object> target) {
	target->Set(String::NewSymbol("insert"), FunctionTemplate::New(Insert)->GetFunction());
	target->Set(String::NewSymbol("find"), FunctionTemplate::New(Find)->GetFunction());
}

NODE_MODULE(bst, init)
